const { fetchJSON } = require('../../common/stuff');
const { printTimeElapsedInSeconds, printTimes } = require("../../common/utils");
const path = require('path');
const execFile = require("child_process").execFile;

console.log(`Main program: ${process.pid}`);

function execFilePromisify(i) {
  return new Promise(function (resolve, reject) {
    const file = `./index${i}.js`;
    const child = execFile( 'node', [`${path.join(__dirname, file)}`], function(error, stdout, stderr) {
        if (error) {
          console.error("stderr", stderr);
          reject(error);
        }

        console.log("stdout", stdout);
        resolve(stdout);
      }
    );
  })
}

const ports = [1234, 1235, 1236];
ports.forEach(function(v, index) {
  execFilePromisify(index + 1)
    .catch(function (error) { console.log(error); });
});

setTimeout(function () {
  Promise.all([
    fetchJSON(`http://localhost:${ports[0]}`),
    fetchJSON(`http://localhost:${ports[1]}`),
    fetchJSON(`http://localhost:${ports[2]}`)
  ]).then(function ([first, _, last]) {
    console.log("This line will be printed after return from Promise.all above");
    const start = new Date(first.start);
    const end = new Date(last.end);

    printTimeElapsedInSeconds(start, end, "Total:");
    printTimes(start, end);
  });
}, 1000);
