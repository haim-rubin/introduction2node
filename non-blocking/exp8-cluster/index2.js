const fetch = require('node-fetch');
const numCPUs = require('os').cpus().length;
const { printTimeElapsedInSeconds } = require("../../common/utils");
const moment = require('moment')
const port = 8000


const commandLineArgs = require('command-line-args')


const optionDefinitions = [
    { name: 'callCount', alias: 'c', type: Number }
  ]
const options = commandLineArgs(optionDefinitions)

const callCount = options.callCount || 1
const start = moment()
const bulk = []
for(var i = 0; i < callCount; i++){
    bulk.push(fetch(`http://localhost:${port}`));
}

console.log(`Running ${callCount}`)


const counts = {};

Promise
    .all(
        bulk
        .map(call => call.then(res => res.json()))
        .map(call => call.then(data => {
            return data
        }))
        )
    .then(res => {
        const counts = 
            res
                .reduce((acc, call) =>{
                    if(!acc[call.pid]){
                        acc[call.pid] = 0;
                    }
                    acc[call.pid]++;
                    return acc
                }, {});
        console.log(counts)
        const end = moment()
        //printTimeElapsedInSeconds(start, end)
        console.log(end.diff(start, 'seconds'))
    })
    .catch((err) =>{ console.log(err)})


    /*
    
    
Concurrent Connections	1	2	4	8	16
Single Process	654	711	783	776	754
8 Workers	594	1198	2110	3010	3024
concurrent requests?

    
    */