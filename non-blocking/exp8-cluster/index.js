const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
const fetch = require('node-fetch');
const port = 8000;


const commandLineArgs = require('command-line-args')


const optionDefinitions = [
    { name: 'workersCount', alias: 'w', type: Number }
  ];

const options = commandLineArgs(optionDefinitions);

const workersCount = options.hasOwnProperty('workersCount')? options.workersCount : numCPUs;

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < workersCount; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  http.createServer((req, res) => {
    res.writeHead(200);
    const { pid } = process
    res.end(`{"message": "hello world from process ${pid}", "pid": "${pid}"}`);
  }).listen(port);

  console.log(`Worker ${process.pid} started`);
}

