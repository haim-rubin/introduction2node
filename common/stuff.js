const { mesure, printTimeElapsedInSeconds, printTimes } = require("./utils");
const fetch = require('node-fetch');
const moment = require('moment');
function wait2Seconds() {
  const { start, end } = mesure();
  printTimeElapsedInSeconds(start, end)
  printTimes(start, end);
  return { start, end };
}

//Lets promisify it
function wait2SecondsPromisify(tag) {
  return new Promise(function (resolve) {
    const res = wait2Seconds(tag);
    resolve(res);
  });
}

const fetchJSON = function(url) {
  return fetch(url).then(res => res.json());
};

module.exports = { wait2Seconds, wait2SecondsPromisify, fetchJSON };
