const moment = require('moment');

const TIME_DEFAULT_FORMAT = 'HH:mm:ss';

function mesure() {
  const start = moment();
  let end;
  do{
    end = moment()
  }while(end.diff(start, 'seconds') < 2)
 
  return { start, end };
}

function getTimeElapsed(start, end, format = TIME_DEFAULT_FORMAT) {
  return moment
    .utc(
      moment(end, "DD/MM/YYYY HH:mm:ss")
        .diff(
          moment(start, "DD/MM/YYYY HH:mm:ss")
        )
    )
    .format(format);
}

function printTimeElapsedInSeconds(start, end, remark = '') {
  const timeElapsed = getTimeElapsed(start, end, 'ss');
  console.log(`${remark} ${parseInt(timeElapsed)} seconds elapsed.`);
}

function printTimes(start, end) {
  console.log(
    `${moment(start).format(TIME_DEFAULT_FORMAT)} - ${moment(end).format(TIME_DEFAULT_FORMAT)}`
);
}


module.exports = {
  mesure,
  getTimeElapsed,
  printTimeElapsedInSeconds,
  printTimes
};