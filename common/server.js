const express = require("express");
const app = express();
const { wait2SecondsPromisify } = require('./stuff');

function server(port) {
  app.get("/", function (req, res) {
    console.log(`Server (localhost:${port}), processId: ${process.pid}`);
    wait2SecondsPromisify()
      .then(function (value) {
          res.json(value);
      })
  });

  app.listen(port);
}

module.exports = server