const { wait2SecondsPromisify } = require("../../common/stuff");
const { getTimeElapsed, printTimeElapsedInSeconds } = require('../../common/utils');


let first, last
wait2SecondsPromisify("exp3.1")
  .then(function(res) {
    first = res;
  })
  .then(wait2SecondsPromisify("exp3.2"))
  .then(
  wait2SecondsPromisify("exp3.3")
    .then(function (res) {
      last = res;
    })
  )
  .then(function() {
    console.log(
      "This line will be printed after return from all 3 wait2SecondsPromisify above"
    );
    printTimeElapsedInSeconds(first.start, last.end, 'Total:');
  });
