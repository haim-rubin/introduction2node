const { wait2SecondsPromisify } = require("../../common/stuff");
const { printTimeElapsedInSeconds } = require("../../common/utils");


Promise.all([
  wait2SecondsPromisify("exp3.1"),
  wait2SecondsPromisify("exp3.2"),
  wait2SecondsPromisify("exp3.3")
])
  .then(function ([first, _, last ]) {
    console.log("This line will be printed after return from Promise.all above");
     printTimeElapsedInSeconds(first.start, last.end, "Total:");
  });
