const server = require('../../common/server')
const { fetchJSON } = require('../../common/stuff');
const { printTimeElapsedInSeconds } = require("../../common/utils");

const ports = [1234, 1235, 1236];
server(ports[0]);
server(ports[1]);
server(ports[2]);

 Promise.all([
    fetchJSON(`http://localhost:${ports[0]}`),
    fetchJSON(`http://localhost:${ports[1]}`),
    fetchJSON(`http://localhost:${ports[2]}`)
  ]).then(function ([first, _, last]) {
    console.log("This line will be printed after return from Promise.all above");
    printTimeElapsedInSeconds(new Date(first.start), new Date(last.end), "Total:");
  });