const { wait2SecondsPromisify } = require("../../common/stuff");

wait2SecondsPromisify('exp2')
  .then(function () {
    console.log("This line will be printed after return from function above");
  });